# terraform-cloud-demo

Demonstration of Terraform Cloud and GitLab Integration.  This Terraform configuration provisions an EC2 instance in AWS
using the Terraform Cloud [UI/VCS-driven run workflow](https://www.terraform.io/docs/cloud/run/ui.html).

## Details
By default, this configuration provisions an Ubuntu 18.04 Base Image AMI (with ID ami-085925f297f89fce1) with type t2.micro in the us-east-1 region. The AMI ID, region, and type can all be set as variables. You can also set the name variable to determine the value set for the Name tag.

Note that you need to set environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.

## Getting Started
### Fork and Clone this project from GitLab
First you'll need to create an account on [GitLab.com](https://gitlab.com/users/sign_in). Once you've
created an account clone or fork the repository. 
```bash
git clone https://gitlab.com/sms-pub/terraform-cloud-demo.git
cd terraform-cloud-demo
```

### Set up Terraform Cloud and GitLab integration
Follow the [Getting Started](https://learn.hashicorp.com/terraform/cloud-getting-started/signup) guide for Terraform Cloud.
Return to this README after you've set up your Workspace.

### Install Terraform CLI
Go to the [Terraform Downloads](https://www.terraform.io/downloads.html) page and download the latest version of the 
Terraform CLI.
```bash
wget https://releases.hashicorp.com/terraform/0.12.29/terraform_0.12.29_linux_amd64.zip
unzip terraform_0.12.29_linux_amd64.zip
chmod 755 terraform
sudo mv terraform /usr/local/bin/
sudo chown root:root /usr/local/bin/terraform
rm -rf terraform_0.12.29_linux_amd64.zip
```

Update variables.tf to use your current terraform client version and Terraform Workspace if needed.
```hcl-terraform
terraform {
  required_version = var.tform_client_version
    backend "remote" {
    organization = var.organization

    workspaces {
      name = var.tform_workspace
    }
  }
}
```

Login to Terraform Cloud and initialize plugins.  This will create a User API key and set up the local environment to 
to use the Terraform Cloud [remote backend](https://www.terraform.io/docs/cloud/run/cli.html#remote-backend-configuration)
```bash
terraform login
terraform init
terraform get
```

### Configure Variables
You can use the included variable scripts in the scripts directory or one of the other methods listed [below](#adding-variables-to-terraform-cloud).  

Update the included variables.csv or create a secrets.csv
```bash
cd scripts
vi secrets.csv
```
```
AWS_ACCESS_KEY_ID;<KEY_ID>;env;false;true;This is a secure value
AWS_SECRET_ACCESS_KEY;<KEY>;env;false;true;This is a secure value
```
Set required ENV variables. Use the [API Token](https://www.terraform.io/docs/cloud/users-teams-organizations/users.html#api-tokens) that
was created when running `terraform login` in the [Install Terraform CLI](#install-terraform-cli) section.
```
export TFE_TOKEN=<user_token>
export TFE_ORG=sms
```
Run the set-variables.sh script
```
~$ ./set-variables.sh terraform-cloud-demo secrets.csv 
TFE_TOKEN environment variable was found.
TFE_ORG environment variable was set to sms.
Using organization, sms.
TFE_ADDR environment variable was not set.
Using Terraform Cloud (TFE SaaS) address, app.terraform.io.
If you want to use a private TFE server, export/set TFE_ADDR.
Using workspace:  terraform-cloud-demo
Found and using secrets.csv file provided in second argument
Checking to see if workspace exists and getting workspace ID
Workspace ID:  <id>

Setting env variable AWS_ACCESS_KEY_ID with value <removed>, hcl: false, sensitive: true, with a description of 
Set all variables.
```

### Run Speculative plans with Terraform CLI
Make sure you're in the root of the repository and run
```bash
terraform plan
```
### Commit changes to GitLab
If the plan succeeds commit any Terraform Config changes you've made back to GitLab.
```bash
git commit -a
git push
```
### Confirm & Apply plan in Terraform Cloud
Once the configuration is pushed to GitLab a Terraform Plan will run in the Terraform Cloud Workspace.  You can revie the
results of the Plan by going to the CI/CD section of the project in GitLab or to the Workspace in Terraform Cloud. 

If the plan succeeds, the current run in the Terraform Cloud Run List will be marked with "! NEEDS CONFIRMATION"

To execute the Terraform Apply open the run details and select **Confirm & Apply**

If everything goes well you should have see a new instance deployed in your AWS account. You can retrieve the public IP 
of the EC2 instance from the Terraform Cloud Apply output or from the CLI with;
```bash
~$ terraform output
public_dns = ec2-54-145-202-53.compute-1.amazonaws.com
```

### Destroy
Plan actions are not allowed from the CLI in Terraform Cloud VCS backed work spaces so to destroy the newly created EC2 
instance you must use the Terraform Cloud UI.

Got to **Settings** section of the Workspace and select **Queue destroy plan**.

### Adding Variables to Terraform Cloud
There are several options for [adding Variables to Terraform Cloud](https://www.terraform.io/docs/cloud/workspaces/variables.html).

1. Using [Terraform Config Files](https://www.terraform.io/docs/cloud/workspaces/variables.html#loading-variables-from-files) (*.auto.tfvars) 
2. Using the [Terraform Cloud UI](https://www.terraform.io/docs/cloud/workspaces/variables.html#managing-variables-in-the-ui)
3. Using the [Terraform Cloud Provider](https://www.terraform.io/docs/providers/tfe/r/variable.html)
4. Using the [Terraform Cloud API](https://www.terraform.io/docs/cloud/api/workspace-variables.html)
5. Using [Client Libraries, Tools and Scripts](https://www.terraform.io/docs/cloud/api/workspace-variables.html)
